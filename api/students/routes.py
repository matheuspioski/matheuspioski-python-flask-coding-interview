from typing import Optional

from pydantic import BaseModel

from flask_openapi3 import APIBlueprint
from sqlalchemy import select

from database import db
from api.students.models import Student

from flask import request, jsonify

students_app = APIBlueprint("students_app", __name__)




@students_app.post("/students")
def create_student():
    data = request.json
    student = Student(**data)
    db.session.add(student)
    db.session.commit()
    return jsonify("Student sucessfully created!"), 200


@students_app.post("/students/<int:student_id>")
def get_student(student_id):
    student = db.session.query(Student).filter_by(student_id).first()
    if not student:
        return jsonify("Student not found"), 404
    return jsonify(student), 200